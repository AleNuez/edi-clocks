<?php session_start();?>
<!DOCTYPE html>
<html lang="es">
<body onload='document.form1.text1.focus()'>
<?php include("db.php"); ?>

<?php include('includes/header.php'); ?>

<main class="container p-4">
  <div class="row">
    <div class="col-md-4">
      <!-- MESSAGES -->

      <?php if (isset($_SESSION['message'])) { ?>
      <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message']?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php session_unset(); } ?>

      <!-- ADD TASK FORM -->
      <div class="card card-body">
        <form name="principal" action="save_task.php" method="POST">
          <div class="form-group">
            <input type="text" name="ip" class="form-control" placeholder="IP del Reloj" onchange="ValidateIPaddress(document.principal.ip)">
          </div>
          <div class="form-group">
            <textarea name="description" rows="2" class="form-control" placeholder="Ubicación del Reloj"></textarea>
          </div>
          <input type="submit" name="save_task" class="btn btn-success btn-block" value="Registrar Reloj">
          
        </form>
      </div>
      <script src="js/ipaddress-validation.js"></script>
    </div>
    <div class="col-md-8">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Dirección IP</th>
            <th>Ubicación</th>
            <th>Fecha de Alta</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $query = "SELECT * FROM Clock";
          $result_tasks = mysqli_query($conn, $query);    

          if ($result_tasks){
          while($row = mysqli_fetch_assoc($result_tasks)) { ?>
          <tr>
            <td><?php echo $row['Ip_clock']; ?></td>
            <td><?php echo $row['Location_clock']; ?></td>
            <td><?php echo $row['Date_clock']; ?></td>
            <td>
              <a href="edit.php?id=<?php echo $row['Id_Clock']?>" class="btn btn-secondary">
                <i class="fas fa-marker"></i>
              </a>
              <a href="delete_task.php?id=<?php echo $row['Id_Clock']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
              </a>
              <a href="search_find.php?id=<?php echo $row['Ip_clock']?>" class="btn btn-secondary">
                <i class="fas fa-calendar-alt"></i>
              </a>
            </td>
          </tr>
          <?php }} ?>
        </tbody>
      </table>
    </div>
  </div>
  
</main>

<?php include('includes/footer.php'); ?>
</body>
</html>